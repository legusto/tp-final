document.addEventListener("DOMContentLoaded", function(event) {


    var connexion = new MovieDb();


    if(document.location.pathname.search("ficheArticle.html") > 0){

        let params = ( new URL(document.location) ).searchParams;

        // console.log( params.get("id") );
        connexion.requeteInfoFilm( params.get("id") );
        connexion.requeteCredit( params.get("id") );
    }
    else{
        connexion.requetePresentement();
        connexion.requetePopulaire();
    }



    //Un commentaire
    console.log("Ça fonctionne!!!");



    //initialize swiper when document ready


    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 3,
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });





    function scrollToTop() {
        var position =
            document.body.scrollTop || document.documentElement.scrollTop;
        if (position) {
            window.scrollBy(0, -Math.max(1, Math.floor(position / 10)));
            scrollAnimation = setTimeout("scrollToTop()", 30);
        } else clearTimeout(scrollAnimation);
    }


});





class MovieDb {

    constructor() {

        this.APIkey = "22a055d30fbfb3b4477630e5782d6097";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 9;

        this.totalArticle = 6;

        this.totalActeur = 4;

        this.totalTeteAffiche = 3;

    }//fin constructor


    requetePresentement() {


        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequetePresentement.bind(this));

        xhr.open("GET", this.baseURL + "movie/now_playing?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();

    }//fin requetePresentement


    retourRequetePresentement(e) {
        let target = e.currentTarget;
        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.afficherPresentement(data);

        }
    }//fin retourRequete


    afficherPresentement(data) {

        for (let i = 0; i < this.totalFilm; i++) {

            //AFFICHAGE DE DONNÉES
            console.log('backdrop_path ' + data[i].backdrop_path);
            console.log('release_date ' + data[i].release_date);
            console.log('id ' + data[i].id);
            console.log('title ' + data[i].title);
            console.log('overview ' + data[i].overview);
            console.log('vote_average ' + data[i].vote_average);


            let unArticle = document.querySelector(".template>div.swiper-slide").cloneNode(true);


            unArticle.querySelector('h3').innerHTML = data[i].title;
            unArticle.querySelector('h4').innerHTML = data[i].vote_average + "/10";




            unArticle.querySelector('img').setAttribute("src", this.imgPath + "w" + this.largeurAffiche[5] + data[i].poster_path);


            // let unLien = unArticle.querySelector("a");
            // unLien.setAttribute("href", "ficheArticle");
            //
            let lienFilm = unArticle.querySelector("a");
            lienFilm.setAttribute("href", "ficheArticle.html?id=" + data[i].id);


            document.querySelector("div.swiper-wrapper").appendChild(unArticle);


        }

    }//fin afficherPresentement






    //DÉBUT POPULAIRE

    requetePopulaire() {


        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourPopulaire.bind(this));

        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();

    }//fin requetePresentement


    retourPopulaire(e) {
        let target = e.currentTarget;
        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.afficherPopulaire(data);

        }
    }//fin retourRequete


    afficherPopulaire(data) {

        for (let i = 0; i < this.totalArticle; i++) {

            console.log(data[i].title);

            //selection de l'article en template et le met dans une variable
            let unArticle = document.querySelector(".template>article.film").cloneNode(true);


            //met le title [attribut sur movieDB] dans la balise h2
            unArticle.querySelector("h2").innerHTML = data[i].title;
            unArticle.querySelector(".etoile").innerHTML = data[i].vote_average;
            unArticle.querySelector(".year").innerHTML = data[i].release_date;

            //s'il n'y a pas de description dans la base de donnée
            if (data[i].overview == "") {
                unArticle.querySelector(".description").innerHTML = "Pas encore de description :(";
            }
            //si il y a une description dans la base de donnée
            else {
                unArticle.querySelector(".description").innerHTML = data[i].overview;
            }

            //isolation dans une variable de la balise img à l'intérieur de l'article dans template
            let uneImage = unArticle.querySelector("img");

            //changement de la source pour le chemin vers l'image ayant le id de l'article choisit
            //[ this fait reference au methodes de l'objet dans lequel on es ]
            uneImage.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);


            let unLien = unArticle.querySelector("a");
            unLien.setAttribute("href", "ficheArticle");

            //selection d'e la balise a de l'article dans le template
            let lienFilm = unArticle.querySelector("a");

            //changement de l'attribut href pour lui donner le chemin de la fiche film avec le id retourné par la BD
            lienFilm.setAttribute("href", "ficheArticle.html?id=" + data[i].id);

            //Mets ce qui est fait dans le template à l'intérieur de notre liste
            document.querySelector(".liste-films").appendChild(unArticle);
        }

    }//FIN POPULAIRE





















    requeteInfoFilm(idFilm) {
        // console.log('id passé avec succès');


        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteInfoFilm.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + idFilm + "?language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();
    }


    retourRequeteInfoFilm(e) {
        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);


            this.afficheInfoFilm(data);

        }
    }//fin retourRequete


    //changer les info de la page html fiche-film
    afficheInfoFilm(data) {

        // console.log(data.id);
        // console.log(data.release_date);
        // console.log(data.overview);
        //
        // console.log(data.budget);
        // console.log(data.revenue);
        // console.log(data.original_language);
        // console.log(data.runtime);


        //la requete get credit pour afficher les acteur va se faire ici - a l'interieur de l'Affichage des info

        let uneFiche = document.querySelector(".template>article.ficheArticle").cloneNode(true);

        let uneImageFilm = uneFiche.querySelector("img.imageFilm");
        uneImageFilm.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[5] + data.poster_path);

        uneFiche.querySelector("h2.titre").innerHTML = data.title;
        uneFiche.querySelector("h3.etoiles").innerHTML = data.vote_average;
        uneFiche.querySelector("h3.date").innerHTML = data.release_date;

        uneFiche.querySelector("h3.budget").innerHTML = " &nbsp " + data.budget + " $";
        uneFiche.querySelector("h3.recette").innerHTML = " &nbsp " + data.revenue + " $";
        uneFiche.querySelector("h3.duree").innerHTML = " &nbsp " + data.runtime + " minutes";

        uneFiche.querySelector("h3.langOriginale").innerHTML = data.original_language;


        //TOUTE LES LANGUES POSSIBLES AUQUELLE JAI PENSER ET QUE JAI VERIFIER LE LANG CODE
        if (data.original_language == 'en') {
            uneFiche.querySelector("h3.langOriginale").innerHTML = ' Langue originale : Anglais';
        }
        else if (data.original_language == 'fr') {
            uneFiche.querySelector("h3.langOriginale").innerHTML = " Langue originale : Français";
        }
        else if (data.original_language == 'cn') {
            uneFiche.querySelector("h3.langOriginale").innerHTML = " Langue originale : Chinois";
        }
        else if (data.original_language == 'de') {
            uneFiche.querySelector("h3.langOriginale").innerHTML = " Langue originale : Allemand";
        }
        else if (data.original_language == 'it') {
            uneFiche.querySelector("h3.langOriginale").innerHTML = " Langue originale : Italien";
        }
        else if (data.original_language == 'pt') {
            uneFiche.querySelector("h3.langOriginale").innerHTML = " Langue originale : Portuguais";
        }
        else if (data.original_language == 'es') {
            uneFiche.querySelector("h3.langOriginale").innerHTML = " Langue originale : Espagnol";
        }
        else if (data.original_language == 'nl') {
            uneFiche.querySelector("h3.langOriginale").innerHTML = " Langue originale : Néerlandais";
        }
        else {
            uneFiche.querySelector("h3.langOriginale").innerHTML = data.original_language;
        }


        let unSynopsis = uneFiche.querySelector("div.synopsis>h4");

        if (data.overview == "") {
            unSynopsis.innerHTML = "Il semblerait qu'il n'y ait pas de synopsis en français pour l'instant, désolé :(";
        } else {
            unSynopsis.innerHTML = data.overview;

        }


        document.querySelector(".sectionFicheFilm").appendChild(uneFiche);

    } //Fin afficherInfoFilm



















    //REQUETE POUR TETES D'AFFICHE

    requeteCredit(idFilm) {
        // console.log(idFilm);

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteCredit.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + idFilm + "/credits?api_key=" + this.APIkey);

        xhr.send();
    }


    retourRequeteCredit(e) {
        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).cast;

            console.log(data);

            this.afficheCredit(data);
        }

    }


    afficheCredit(data){

        for (let i = 0; i < this.totalTeteAffiche; i++){

        console.log(data[i].profile_path);

        let ficheArticle = document.querySelector(".template>article.ficheArticle");
        let uneTeteDaffiche = ficheArticle.querySelector("div.teteAffiche").cloneNode(true);

        uneTeteDaffiche.querySelector('img').setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].profile_path);

        // uneTeteDaffiche.querySelector('img').setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].profile_path);
        //
        document.querySelector('.sectionTete').appendChild(uneTeteDaffiche);
        }


    }

    //FIN REQUETE POUR TETES D'AFFICHE



}//fin classe MovieDb
